# gets todays date
$todaysDate = Get-Date -format yyyyMMdd

<# replace '\\server\path' with the folder you are copying #>
$sourceFolder = "%systemroot%"

# initializes the destination usb -- change 'HD2USB' to the name of the destination usb device
$usbdrive=(GWmi Win32_LogicalDisk | ?{$_.VolumeName -eq 'HD2USB'} | %{$_.DeviceID})

if (!(Test-Path -path $sourceFolder)) {New-Item $sourceFolder -Type Directory}

# create destination folder
New-Item -Name newfolder -ItemType directory -Destination $usbdrive

# change below path to the folder/file you want to be copied
Copy-Item -Path C:\Users\Peter\Desktop\TEST-FOLDER\*.txt -Destination $usbdrive

<# this moves files instead of copying, uncomment if you prefer this

Get-ChildItem -Path $usbdrive -Recurse -Include *.docs | Move-Item -Destination $sourceFolder -verbose

Write-Host "The file sync is complete. Press any key to continue."

<#$x = $host.UI.RawUI.ReadKey("NoEcho,IncludeKeyDown")#>